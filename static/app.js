    // create the module and name it scotchApp
    var scotchApp = angular.module('scotchApp', ['ngRoute','rzModule','ngAnimate', 'ui.bootstrap','ui',"ui.bootstrap.modal", 'ui.grid', "chart.js"]);

    // configure our routes
    scotchApp.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'static/templates/index.html',
                //controller  : 'mainController'
            })

            // route for the about page
            .when('/about', {
                templateUrl : 'static/templates/about.html',
                controller  : 'aboutController'
            })

            // route for the contact page
            .when('/login', {
                templateUrl : 'static/templates/login.html',
                controller  : 'loginController'
            })
        
        	// route for the contact page
          .when('/portfolio', {
            templateUrl : 'static/templates/portfolio.html',
            controller  : 'ptController'
          })
          
          .when('/dashboard', {
            templateUrl : 'static/templates/dashboard.html',
            controller  : 'BarCtrl'
          });
    });
    scotchApp.controller('ptController',function($scope,$http){
  //       $scope.myData = [
  //   {
  //       firstName: "Cox",
  //       lastName: "Carney",
  //       company: "Enormo",
  //       employed: true
  //   },
  //   {
  //       firstName: "Lorraine",
  //       lastName: "Wise",
  //       company: "Comveyer",
  //       employed: false
  //   },
  //   {
  //       firstName: "Nancy",
  //       lastName: "Waters",
  //       company: "Fuelton",
  //       employed: false
  //   }
  // ];
  $scope.gridOptions = {
    saveFocus: false,
    saveScroll: true,
    saveGroupingExpandedStates: true,
    
    columnDefs: [
      { name: 'SchemeName' }
    ],
    onRegisterApi: function(gridApi){
      $scope.gridApi = gridApi;
    }
  };
  $scope.gridOptions2 = {
    saveFocus: false,
    saveScroll: true,
    saveGroupingExpandedStates: true,
    
    columnDefs: [
      { name: 'schemeName' },
      {name : 'totalQty'}
    ],
    
  };
$http({method:'GET',
      url:'/api/v1/auth/orderbook/'}).then(function(response) {
       $scope.gridOptions.data =response.data.data;
      })
$http({method:'GET',
      url:'/api/v1/auth/portfolio/'}).then(function(response) {
       $scope.gridOptions2.data =response.data.data.portfolioList;
      })

    });

    // create the controller and inject Angular's $scope
    scotchApp.controller('mainController', function($scope, $http, $timeout,$location,$rootScope, $uibModal) {
    	
    	
        // create a message to display in our view
  //         $scope.initFilters = function() {
  //   $scope.filters = {};
  //   $scope.filters.price = {};
  //   $scope.filters.price.min = 0;
  //   $scope.filters.price.max = 0;
  //   $scope.filters.price.range = [0, 0];
  //   console.log($scope);
  // };
  // $scope.initFilters();

  // $timeout(function() { // Simulate HTTP Call
  //   $scope.initFilters();
  //   $scope.filters.price = {};
  //   $scope.filters.price.min = 0;
  //   $scope.filters.price.max = 100;
  //   $scope.filters.price.range = [$scope.filters.price.min, $scope.filters.price.max];
  //   //$timeout(function() { // Simulate HTTP Call
  //   //  $scope.$broadcast("applyFilters");
  //   //}, 1000);
  // }, 5000);
    if(!$rootScope.authority){
      $location.url('/login');
    }
    $scope.d_data=[];
    $scope.all_data=[];
    $scope.filtered=[];
    $scope.amc=[];
    $scope.ratingmax=5;
    $scope.rate=5;
    
     $http({
      method:'GET',
      url:'/top5schemes/'}).then(function(response) {
        console.log(response);
        

        $scope.d_data = response.data.data;
        
    });
    $scope.$watchCollection('rate',function(){
      $scope.schemesfilter();
    });
    $scope.rateclick=function(c){
      $scope.rate=c;
    };
    $scope.page_size=10;
    $scope.pages=1;
    $scope.currentpage=1;
    $scope.selecTypemf=[];
    $scope.toggleSelectionmf=function(scheme_type){
      if ($scope.selecTypemf.indexOf(scheme_type)>-1) {
        var idx = $scope.selecTypemf.indexOf(scheme_type);
        $scope.selecTypemf.splice(idx, 1);
      }
      else{
        $scope.selecTypemf.push(scheme_type);
      }
       $scope.schemesfilter();
    };
    $scope.schemesfilter=function(){
      $scope.currentpage=1;
        $scope.filtered=[]
        for (var i = $scope.all_data.length - 1; i >= 0; i--) {
           if($scope.selectedamc.indexOf( $scope.all_data[i].amc)>-1){
            $scope.filtered.push($scope.all_data[i]);
           }
        }
        $scope.filtered2=[]
        for (var i = $scope.filtered.length - 1; i >= 0; i--) {
           if($scope.selecTypemf.indexOf( $scope.filtered[i].scheme_type)>-1){
            $scope.filtered2.push($scope.filtered[i]);
           }
        }
        $scope.filtered= angular.copy( $scope.filtered2);
        $scope.filtered2=[];
        for (var i = $scope.filtered.length - 1; i >= 0; i--) {
           if(!Number.isNaN($scope.filtered[i].ratings)){
            if($scope.rate<=parseInt($scope.filtered[i].ratings)){


              $scope.filtered2.push($scope.filtered[i]);

          }
          else{
            if ($scope.rate==0) {
                $scope.filtered2.push($scope.filtered[i]);              
            }
           }
          }
        }
        $scope.filtered= angular.copy( $scope.filtered2);
        $scope.filtered2=[];


        $scope.d_data=$scope.filtered.slice(($scope.currentpage-1)*10,$scope.currentpage*10-1);
    };
    $scope.$watchCollection('all_data', function() { 
      
      if($scope.all_data.length >0){
        $scope.typemf=[];
        
        $scope.pages=$scope.all_data.length/$scope.page_size+1;
        $scope.d_data=$scope.all_data.slice(($scope.currentpage-1)*10,$scope.currentpage*10-1);
        for (var i = $scope.all_data.length - 1; i >= 0; i--) {
          if($scope.typemf.indexOf($scope.all_data[i].scheme_type)<0){
            $scope.typemf.push($scope.all_data[i].scheme_type)

          }
          if($scope.amc.indexOf($scope.all_data[i].amc)<0){
            $scope.amc.push($scope.all_data[i].amc);


            }
              
          // if($scope.ratings.indexOf($scope.all_data[i].ratings<0)){
          //   $scope.ratings.push($scope.all_data[i].ratings)

          // }
        }
        $scope.selectedamc=angular.copy($scope.amc);
        $scope.selecTypemf=angular.copy($scope.typemf);
        console.log($scope.typemf)

      }
      });
     var countUp = function() {
       $http({method:'GET',url:"/allschemes/"})
    .then(function(response) {
        //console.log(response);
        

        $scope.filtered = $scope.all_data = response.data.data;
        
    });
        //$timeout(countUp, 2000);
    $scope.prevPage=function () {
        if($scope.currentpage>1){
        $scope.currentpage=$scope.currentpage-1;
        $scope.d_data=$scope.filtered.slice(($scope.currentpage-1)*10,$scope.currentpage*10-1)}
    }
    $scope.nextPage=function () {
        if($scope.currentpage<$scope.pages){
        $scope.currentpage=$scope.currentpage+1;
        $scope.d_data=$scope.filtered.slice(($scope.currentpage-1)*10,$scope.currentpage*10-1)
    }}

    };
    $scope.selectedamc=[];
    $scope.amc=[];
    $timeout(countUp, 2000);
    $scope.fundlistactive="none";
    $scope.typemf=[];
    $scope.fundcat=["Debt","Equity","Others"];
    $scope.ratings=[1,2,3,4,5];
    $scope.ratingsactive="none";
    $scope.fundcatactive="none";
    $scope.typemfactive="none";
    $scope.fundsshow=function () {
        if ($scope.fundlistactive=="block") {
            $scope.fundlistactive="none";

        }
        else{
            $scope.fundlistactive="block";
        }
    }
    $scope.typemfshow=function () {
        if ($scope.typemfactive=="block") {
            $scope.typemfactive="none";

        }
        else{
            $scope.typemfactive="block";
        }
    }
    $scope.fundcatshow=function () {
        if ($scope.fundcatactive=="block") {
            $scope.fundcatactive="none";

        }
        else{
            $scope.fundcatactive="block";
        }
    }
    $scope.ratingsshow=function () {
        if ($scope.ratingsactive=="block") {
            $scope.ratingsactive="none";

        }
        else{
            $scope.ratingsactive="block";
        }
    }
      $scope.toggleSelection = function toggleSelection(amcname) {
    var idx = $scope.selectedamc.indexOf(amcname);

    // Is currently selected
    if (idx > -1) {
      $scope.selectedamc.splice(idx, 1);
    }

    // Is newly selected
    else {
      $scope.selectedamc.push(amcname);
    }
    //console.log($scope.selectedamc);
     $scope.schemesfilter();
    // if($scope.selectedamc.length>0){
    //     $scope.currentpage=1;
    //     $scope.filtered=[]
    //     for (var i = $scope.all_data.length - 1; i >= 0; i--) {
    //        if($scope.selectedamc.indexOf( $scope.all_data[i].amc)>-1){
    //         $scope.filtered.push($scope.all_data[i]);
    //        }
    //     }
    //     $scope.d_data=$scope.filtered.slice(($scope.currentpage-1)*10,$scope.currentpage*10-1);
    // }
    // else{
    //     $scope.currentpage=1;
    //     $scope.filtered=$scope.all_data;
    //     $scope.d_data=$scope.filtered.slice(($scope.currentpage-1)*10,$scope.currentpage*10-1);
    // }
  };
  		
  		//========== mutual fund buy order modal controller
  		//========== submit form data using post method
  		
        $scope.open = function(mf) {
          var modalInstance = $uibModal.open({
            templateUrl:"static/templates/mfbuy.html",  
            size:'lg',
            keyboard :true,
            controller: function($scope, $uibModalInstance, $rootScope) {
            			  $rootScope.mutf=mf;
                          $scope.cancel = function () {
                        	  $uibModalInstance.close();
                          };
                          $scope.confirm = function(){
                        	  $rootScope.amount = $scope.amount;
                        	  //$rootScope.limit = $scope.limit;
                        	  $uibModalInstance.close();                        	  
                        	  var modalInstance = $uibModal.open({
                        		  templateUrl:"static/templates/confirm.html",  
                                  size:'sm',
                                  keyboard :true,
                                  controller: function($scope, $uibModalInstance,$rootScope) {
                                	  $scope.cancel = function () {
                                    	  $uibModalInstance.close();
                                      };
                                      $scope.submitPurchase = function () {                                 	   
                    	  						$http({ method:'POST',
                    	  								url:'/api/purchaseorder/',  
                    	  								data:{ 
                    	  									'mutf':$rootScope.mutf,
                    	  									'amount':$rootScope.amount
                    	  									//'limit':$scope.limit
                    	  									}
                    	  							}).then(function(response){
                    	  								$scope.purchaseresponse=response.data;
                    	  						});
                    	  					    $uibModalInstance.close();
                                      }; //====== end of submit purchase method
                                  }, //======== end of confirm controller
                        	  });//======== end of confirm modal close
                          };//=========== end of confirm method
                      }, //============ end of purchase modal controller
            resolve:{
            			mutf:function(){
            				return mf;
            				}
            		}
         }); //========= end of purchase modal close
      }; //======== end of function open
      
      
    }); //========= end of mainController 
    
    

    scotchApp.controller('BarCtrl', function($scope, $rootScope) {
    	$scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
    	$scope.data = [50, 30, 20];
    	$scope.colors = ["#C19933", "#30A134", "#33A9D1"];
    	$scope.options = {"segmentShowStroke" : true,
        "animationSteps" : 40,
        "animationEasing" : "linear",
        "drawBorder":"true",
        "cutoutPercentage": 75};
    	
    });
    	
    scotchApp.controller('aboutController', function($scope) {
        $scope.message="ADFASDFASDADSADSAD";
    });
    
    scotchApp.controller('NavbarController', function($scope,$rootScope,loginManager,$http,$location) {
      $scope.username=loginManager.authority;
      $scope.logout=function(){
        $http({method:'GET','url':'/api/v1/auth/logout/'}).then(function(response){
          $rootScope.authority=null;
          $location.url("/login");
        })

      }
        
    });

    scotchApp.controller('loginController', function($scope,$http,$rootScope,$location,loginManager) {
      $scope.loginresponse={};
      $scope.$watchCollection('loginresponse',function(){
        if(typeof $scope.loginresponse.fields!='undefined'){
          if ($scope.loginresponse.auth==true) {
            $rootScope.authority = $scope.loginresponse.fields[0].fields;
       loginManager.setAuth($rootScope.authority); 
       $location.url("/");
          }
        } 
      })
      $http({method:'GET',url:'/api/v1/auth/getauth/'}).then(function(response){
        $scope.loginresponse=response.data;
      });
       $scope.Login=function(){
          //console.log($scope.username_field);
          $http({method:'POST',url:'/api/v1/auth/login/',  data:{ 'username' : $scope.username_field,'password':$scope.password_field }}).then(function(response){
          $scope.loginresponse=response.data;
     
       // $http.get('/api/v1/auth/test/').success(function(data){
       //  console.log(data);
       // });
    });
        }
    });
    
scotchApp.service('loginManager', function () {
    

    this.authority = "csc";

    this.setAuth = function(auth) {
      this.authority =auth;
      console.log(auth);
    };
  });
//    scotchApp.directive('slider', function () {
// return {
//     restrict: 'E',
//     replace: true,
//     transclude: true,
//     template: "<div class='slider'></div>",
//     scope: {
//         min: "=",
//         max: "=",
//         range: "="
//     },
//     link: function (scope, element, attrs) {
//         $(element).slider({
//             range: (scope.range != undefined),
//             min: scope.min,
//             max: scope.max,
//             values: [scope.min, scope.max],
//             slide: function (event, ui) {
//                 scope.$apply(function () {
//                     scope.range = ui.values;
//                 });
//             }
//         });
//         scope.$on("applyFilters", function () {
//             console.log("APPLY FILTERS:", scope);
//             console.log("APPLY FILTERS:", scope.min, scope.max, scope.range);
//         });
//          scope.$watch("min", function(value) {
//       console.log("min ", value);
//       slider.slider( "option", "min", value );
//     });      

//     scope.$watch("max", function(value) {
//       console.log("max ", value);
//       slider.slider( "option", "max", value );
//     });      

//     scope.$watch("range", function(value) {
//       console.log("range ", value);
//       slider.slider( "option", "range", value );
//     });      
//     }
// }
// });
/*   scotchApp.directive('checkRequired', function(){
	  return {
	    require: 'ngModel',
	    restrict: 'A',
	    link: function (scope, element, attrs, ngModel) {
	      ngModel.$validators.checkRequired = function (modelValue, viewValue) {
	        var value = modelValue || viewValue;
	        var match = scope.$eval(attrs.ngTrueValue) || true;
	        return value && match === value;
	      };
	    }
	  }; 
	});*/

(function () {
  'use strict';

  angular
    .module('scotchApp')
    .config(config);

  config.$inject = ['$locationProvider'];
angular
    .module('scotchApp')
    .run(run);
    run.$inject = ['$http','$rootScope', '$location'];
  /**
   * @name config
   * @desc Enable HTML5 routing
   */
  function config($locationProvider) {
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
  }
    function run($http,$rootScope, $location) {
      $rootScope.$on('$routeChangeStart', function (event) {

        if (!$rootScope.authority) {
            console.log('DENY');
            //event.preventDefault();
            $location.url('/login');
        }
        
    });
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
  }
})();





