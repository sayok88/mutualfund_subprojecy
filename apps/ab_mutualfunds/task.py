import string

from django.contrib.auth.models import User
from django.utils.crypto import get_random_string

from celery import shared_task
from celery.decorators import task
from .models import *
import datetime
monthdict={"January":1,"February":2,"March":3,"April":4,"May":5,"June":6,"July":7,"August":8,"September":9,"October":10,"November":11,"Decemder":12}
@shared_task
def create_random_user_accounts(total):
    for i in range(total):
        username = 'user_{}'.format(get_random_string(10, string.ascii_letters))
        email = '{}@example.com'.format(username)
        password = get_random_string(50)
        User.objects.create_user(username=username, email=email, password=password)
    return '{} random users created with success!'.format(total)


@shared_task
def add_schemedetails(response):
    try:
        
        ##print     "in try"
        newschemedetail=SchemeDetails()
        if "data" in response:
            response=response["data"]
            ##print     "in data"
            if "schemeDetails" in response:
                schemedetail1=response["schemeDetails"]
                if len(schemedetail1)>0:
                    schemedetail1=schemedetail1[0]
                ##print     "scheme details"
                if "Scheme_Code" in schemedetail1:
                    scheme=Scheme.objects.get(scheme_code=int(schemedetail1["Scheme_Code"]))
                    ##print     "in scheme code"
                    schemedetailtemp=SchemeDetails.objects.filter(scheme=scheme)
                    if schemedetailtemp:
                        newschemedetail=schemedetailtemp.first()
                    else:
                        newschemedetail.scheme=scheme
                    newschemedetail.min_Pur_Amt_FrNewInvest=schemedetail1["Min_Pur_Amt_FrNewInvest"]
                    newschemedetail.Pur_Mul_Amt_FrNewInvest=schemedetail1["Pur_Mul_Amt_FrNewInvest"]
                    newschemedetail.addIn_Inv_Amt_Fr_ExtInvst=schemedetail1["AddIn_Inv_Amt_Fr_ExtInvst"]
                    newschemedetail.add_all_inv_Mul_Fr_ExtInvt=schemedetail1["Add_all_inv_Mul_Fr_ExtInvt"]
                    newschemedetail.sCutoffTimePurchase=schemedetail1["sCutoffTimePurchase"]
                    newschemedetail.sCutoffTimeRedemption=schemedetail1["sCutoffTimeRedemption"]
                    newschemedetail.optionName=schemedetail1["OptionName"]
                    newschemedetail.end_Date=schemedetail1["End_Date"]
                    newschemedetail.end_time=schemedetail1["End_time"]
                    newschemedetail.min_Redemption_Units=schemedetail1["Min_Redemption_Units"]
                    newschemedetail.save()
            if "navdetails" in response:
                ##print     "In navdetails"
                navdetails=response["navdetails"]
                if "CoCode" in navdetails:
                    ##print     "in nav code"
                    newschemedetail.nav=navdetails["nav"]
                    newschemedetail.prevNav=navdetails["prevNav"]
                    newschemedetail.prevAdjNav=navdetails["prevAdjNav"]
                    newschemedetail.changePercent=navdetails["changePercent"]
                    newschemedetail.change=navdetails["change"]
                    prevNavDate=navdetails["prevNavDate"]
                    navDate=navdetails["navDate"]

                    try:
                        d=datetime.datetime.strptime(prevNavDate,"%d/%m/%Y %I:%M:%S %p")
                        newschemedetail.prevNavDate=d
                        if len(navDate)>1:
                            navmaindate=navDate.split(' ')[0]
                            navmaindate=navmaindate.split('/')
                            navmaindate1=datetime.date(int(navmaindate[2]),monthdict[navmaindate[1]],int(navmaindate[0]))
                            newschemedetail.navDate=navmaindate1
                            if len(newschemedetail.navdata)>0:
                                #print     newschemedetail.navdata
                                #print     "6"
                                if str(navmaindate[2]) in newschemedetail.navdata:
                                    #print     "1"
                                    if str(monthdict[navmaindate[1]]) in  newschemedetail.navdata[str(navmaindate[2])]:
                                        #print     "2"
                                        if not str(navmaindate[0]) in  newschemedetail.navdata[str(navmaindate[2])][str(monthdict[navmaindate[1]])]:
                                            newschemedetail.navdata[str(navmaindate[2])][str(monthdict[navmaindate[1]])][str(navmaindate[0])]=navdetails
                                    else:
                                        #print     "3"
                                        newschemedetail.navdata[str(navmaindate[2])][str(monthdict[navmaindate[1]])]={str(navmaindate[0]):navdetails}
                                else:
                                    #print     "4"
                                    newschemedetail.navdata[str(navmaindate[2])]={str(monthdict[navmaindate[1]]):{str(navmaindate[0]):navdetails}}
                            else:
                                #print     "5"
                                newschemedetail.navdata={str(navmaindate[2]):{str(monthdict[navmaindate[1]]):{str(navmaindate[0]):navdetails}}}
                    except Exception as e:
                        print     str(e)
                    newschemedetail.save()
                    '''Code for historical nav to be inserted'''
            if "returns" in response:
                returns=response["returns"]
                if len(returns)>0:
                    returns=returns[0]
                if "mfschemecode" in returns:
                    newschemedetail.returndate=returns["returndate"]
                    newschemedetail.todaysdate=returns["todaysdate"]
                    newschemedetail.todaysnav=str(returns["todaysnav"])
                    newschemedetail.weekfrm1=returns["weekfrm1"]
                    newschemedetail.weeknav1=str(returns["weeknav1"])
                    newschemedetail.week1=str(returns["week1"])
                    newschemedetail.mthfrm1=returns["mthfrm1"]
                    newschemedetail.mthnav1=str(returns["mthnav1"])
                    newschemedetail.mth1=str(returns["mth1"])
                    newschemedetail.mthfrm3=returns["mthfrm3"]
                    newschemedetail.mthnav3=str(returns["mthnav3"])
                    newschemedetail.mth3=str(returns["mth3"])
                    newschemedetail.mthfrm6=returns["mthfrm6"]
                    newschemedetail.mthnav6=str(returns["mthnav6"])
                    newschemedetail.mth6=str(returns["mth6"])
                    newschemedetail.yrfrom1=returns["yrfrom1"]
                    newschemedetail.yrnav1=str(returns["yrnav1"])
                    newschemedetail.year1=str(returns["year1"])
                    newschemedetail.yrfrom3=returns["yrfrom3"]
                    newschemedetail.yrnav3=str(returns["yrnav3"])
                    newschemedetail.year3=str(returns["year3"])
                    newschemedetail.yrfrom5=returns["yrfrom5"]
                    newschemedetail.yrnav5=str(returns["yrnav5"])
                    newschemedetail.year5=str(returns["year5"])
                    newschemedetail.incdate=returns["incdate"]
                    newschemedetail.incnav=str(returns["incnav"])
                    newschemedetail.returnsinceinception=str(returns["returnsinceinception"])
                    newschemedetail.launchdate=returns["launchdate"]
                    newschemedetail.AUM=str(returns["AUM"])
                    newschemedetail.AumDate=returns["AumDate"]
                    newschemedetail.fundcode=str(returns["fundcode"])
                    newschemedetail.Fundmgr=returns["Fundmgr"]
                    newschemedetail.FundType=returns["FundType"]
                    newschemedetail.NoOfStocks=str(returns["NoOfStocks"])
                    newschemedetail.schemeType=returns["schemeType"]
                    newschemedetail.arqScore=str(returns["arqScore"])
                    newschemedetail.crisilRating=str(returns["crisilRating"])
                    newschemedetail.valueRating=str(returns["valueRating"])
                    newschemedetail.mstarRating=str(returns["mstarRating"])
                    newschemedetail.save()
    except Exception as e:
        print str(e)












