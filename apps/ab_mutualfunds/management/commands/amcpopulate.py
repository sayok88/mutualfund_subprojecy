from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from apps.ab_mutualfunds.models import *
import string,requests
class Command(BaseCommand):
    help = 'Populates with latest AMC list'
    def add_arguments(self, parser):
        parser.add_argument('searchstr', nargs='+', type=str)
    def handle(self, *args, **options):
        lookup_str=[]
        if options['searchstr'][0]=='all':
            lookup_str=list(string.ascii_lowercase)
        else:
            lookup_str.append(options['searchstr'][0])
        api_url=settings.ANGEL_BROKING_SVC_URL
        amc_url=api_url+'/mfsearchfund'
        return_output=[]

        for argstr in lookup_str:
            self.stdout.write(self.style.SUCCESS(str(argstr)))
            r=requests.post(amc_url,"{'angelID':'ABH1','searchText':'"+argstr+"'}",verify=False)
            rjson=r.json()
            if "data" in rjson:
                for data in rjson["data"]:
                    return_output.append(data)
                    self.stdout.write(self.style.SUCCESS(str(data)))
        for data in return_output:
            try:
                Amc(amc_code=data["AMC_CODE"],amc_name=data["AMC_NAME"]).save()
            except Exception as e:
                self.stdout.write(self.style.SUCCESS(str(e)))
