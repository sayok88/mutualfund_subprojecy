from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from apps.ab_mutualfunds.models import *
import string,requests
class Command(BaseCommand):
    help = 'Populates with latest AMC list'
    def add_arguments(self, parser):
        parser.add_argument('searchstr', nargs='+', type=str)
    def handle(self, *args, **options):
        lookup_str=[]
        if options['searchstr'][0]=='all':
            lookup_str=list(string.ascii_lowercase)
        else:
            lookup_str.append(options['searchstr'][0])
        api_url=settings.ANGEL_BROKING_SVC_URL
        amc_url=api_url+'/mfsearchscheme'
        return_output=[]
        amcs=Amc.objects.all()
        for amc in amcs:
            for argstr in lookup_str:
                self.stdout.write(self.style.SUCCESS(str(argstr)))
                r=requests.post(amc_url,"{'angelID':'ABH1','searchText':'"+argstr+"','amcCode':'"+amc.amc_code+"'}",verify=False)
                rjson=r.json()
                if "data" in rjson:
                    for data in rjson["data"]:
                        return_output.append(data)
                        self.stdout.write(self.style.SUCCESS(str(data)))
        """{u'PlanName': u'Equity Fund - T3', u'Scheme_Name': u'JM BASIC FUND - Dividend Payout Option', u'PlanCode': u'59', u'Exchange': u'BSE', u'Col
        umn1': 180.54, u'OptionCode': u'DP', u'AMC_Code': u'JMMF', u'CoCode': u'5497', u'Scheme_Code': u'47644', u'ISIN': u'INF137A01011', u'ExchCod
        e': u'3', u'NFO_Flag': u'N ', u'AMC_Scheme_Code': u'BADP', u'OptionWeightAge': 1}"""
        for data in return_output:
            try:
                Scheme(scheme_code=int(data["Scheme_Code"]),plan_name=data["PlanName"],amc_code_id=data["AMC_Code"],amc_scheme_code=data["AMC_Scheme_Code"],
                    scheme_name=data["Scheme_Name"],plancode=data["PlanCode"],optioncode=data["OptionCode"],nfo_flag=data["NFO_Flag"],
                    isin=data["ISIN"],exchange=data["Exchange"],exchange_code=data["ExchCode"],co_code=data["CoCode"],coloumn1=data["Column1"],
                    optionw_weightage=str(data["OptionWeightAge"])).save()
            except Exception as e:
                self.stdout.write(self.style.SUCCESS(str(e)))
