from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from apps.ab_mutualfunds.models import *
from apps.ab_mutualfunds.task import create_random_user_accounts
import string,requests
class Command(BaseCommand):
    help = 'Populates with latest AMC list'
    def add_arguments(self, parser):
        parser.add_argument('searchstr', nargs='+', type=str)
    def handle(self, *args, **options):
        for i in range(1000):
            create_random_user_accounts.delay(1)