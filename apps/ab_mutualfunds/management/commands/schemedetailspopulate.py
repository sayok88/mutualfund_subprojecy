from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from apps.ab_mutualfunds.models import *
import string,requests
from Queue import Queue
from threading import Thread
from time import time, sleep
import sys
import urllib3
from apps.ab_mutualfunds.task import add_schemedetails
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

all_results=[]
api_url=settings.ANGEL_BROKING_SVC_URL
amc_url=api_url+'/mfschemedetails'
q=Queue()
def worker():
    while True:
        item=q.get()
        try:
            r=requests.post(amc_url,"{'schemeCode':'"+str(item[0])+"','exchange':'"+str(item[1])+"'}",verify=False)
            rjson=r.json()
            add_schemedetails.delay(rjson)
        except Exception as e:
            print str(e)

        all_results.append(item[0])
        q.task_done()
class Command(BaseCommand):
    help = 'Populates with latest AMC list'
    def add_arguments(self, parser):
        parser.add_argument('searchstr', nargs='+', type=str)
    def handle(self, *args, **options):
        lookup_str=[]
        if options['searchstr'][0]=='all':
            lookup_str=list(string.ascii_lowercase)
        else:
            lookup_str.append(options['searchstr'][0])
        
        return_output=[]
        schemes=Scheme.objects.all().values_list('scheme_code','exchange_code')
        
        for s in schemes:
            q.put(s)
        workers=100
        for i in range(workers):
            t = Thread(target=worker)
            t.daemon = True
            t.start()
        q.join()
        print all_results



        # for scheme in schemes:
        #     self.stdout.write(self.style.SUCCESS(str(argstr)))
        #     r=requests.post(amc_url,"{'schemeCode':'"+str(scheme.scheme_code)+"','exchange':'"+str(scheme.exchange_code)+"'}",verify=False)
        #     rjson=r.json()
        #     if "data" in rjson:
        #         for data in rjson["data"]:
        #             return_output.append({"scheme_code":scheme.scheme_code,"data":data})
        #             #self.stdout.write(self.style.SUCCESS(str(data)))
        # """{u'PlanName': u'Equity Fund - T3', u'Scheme_Name': u'JM BASIC FUND - Dividend Payout Option', u'PlanCode': u'59', u'Exchange': u'BSE', u'Col
        # umn1': 180.54, u'OptionCode': u'DP', u'AMC_Code': u'JMMF', u'CoCode': u'5497', u'Scheme_Code': u'47644', u'ISIN': u'INF137A01011', u'ExchCod
        # e': u'3', u'NFO_Flag': u'N ', u'AMC_Scheme_Code': u'BADP', u'OptionWeightAge': 1}"""
        # for data in return_output:
        #     try:
        #         Scheme(scheme_code=int(data["Scheme_Code"]),plan_name=data["PlanName"],amc_code_id=data["AMC_Code"],amc_scheme_code=data["AMC_Scheme_Code"],
        #             scheme_name=data["Scheme_Name"],plancode=data["PlanCode"],optioncode=data["OptionCode"],nfo_flag=data["NFO_Flag"],
        #             isin=data["ISIN"],exchange=data["Exchange"],exchange_code=data["ExchCode"],co_code=data["CoCode"],coloumn1=data["Column1"],
        #             optionw_weightage=str(data["OptionWeightAge"])).save()
        #     except Exception as e:
        #         self.stdout.write(self.style.SUCCESS(str(e)))
