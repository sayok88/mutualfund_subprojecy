# -*- coding= utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import datetime

from django.utils import timezone
from django.contrib.postgres.fields import JSONField


# Create your models here.
class Amc(models.Model):
    amc_code=models.CharField(primary_key=True,max_length=10)
    amc_name=models.CharField(default='',max_length=100)
    """docstring for Amc"""
    def __str__(self):
        return str(self.amc_code)
class Scheme(models.Model):
	scheme_code=models.IntegerField(primary_key=True)
	amc_code=models.ForeignKey(Amc)
	amc_scheme_code=models.CharField(default='',max_length=30)
	scheme_name=models.CharField(default='',max_length=200)
	plancode=models.CharField(default='',max_length=30)
	optioncode=models.CharField(default='',max_length=10)
	nfo_flag=models.CharField(default='',max_length=10)
	isin=models.CharField(default='',max_length=20)
	plan_name=models.CharField(default='',max_length=20)
	exchange=models.CharField(default='',max_length=10)
	exchange_code=models.CharField(default='',max_length=10)
	co_code=models.CharField(default='',max_length=30)
	coloumn1=models.CharField(default='',max_length=30,null=True)
	optionw_weightage=models.CharField(default='',max_length=20)
	def __str__(self):
		return str(self.scheme_code)
	
       
class SchemeDetails(models.Model):
    scheme=models.OneToOneField(Scheme,primary_key=True)
    min_Pur_Amt_FrNewInvest=models.CharField(default='',max_length=20)
    Pur_Mul_Amt_FrNewInvest=models.CharField(default='',max_length=20)
    addIn_Inv_Amt_Fr_ExtInvst=models.CharField(default='',max_length=20)
    add_all_inv_Mul_Fr_ExtInvt=models.CharField(default='',max_length=20)
    sCutoffTimePurchase=models.CharField(default='',max_length=20)
    sCutoffTimeRedemption=models.CharField(default='',max_length=20)
    optionName=models.CharField(default='',max_length=20)
    end_Date=models.CharField(default='',max_length=20)
    end_time=models.CharField(default='',max_length=20)
    min_Redemption_Units=models.CharField(default='',max_length=20)
    navDate=models.DateTimeField(null=True)
    nav=models.CharField(default='',max_length=20)
    prevNavDate=models.DateTimeField(null=True)
    prevNav=models.CharField(default='',max_length=20)
    prevAdjNav=models.CharField(default='',max_length=20)
    changePercent=models.CharField(default='',max_length=20)
    change=models.CharField(default='',max_length=20)
    returndate=models.DateField(null=True)
    todaysdate=models.DateField(null=True)
    weekfrm1= models.DateField(null=True)
    weeknav1= models.CharField(default='',max_length=20)
    week1= models.CharField(default='',max_length=20)
    mthfrm1= models.DateField(null=True)
    mthnav1= models.CharField(default='',max_length=20)
    mth1= models.CharField(default='',max_length=20)
    mthfrm3= models.DateField(null=True)
    mthnav3= models.CharField(default='',max_length=20)
    mth3= models.CharField(default='',max_length=20)
    mthfrm6= models.DateField(null=True)
    mthnav6= models.CharField(default='',max_length=20)
    mth6= models.CharField(default='',max_length=20)
    yrfrom1= models.DateField(null=True)
    yrnav1= models.CharField(default='',max_length=20)
    year1= models.CharField(default='',max_length=20)
    yrfrom3= models.DateField(null=True)
    yrnav3= models.CharField(default='',max_length=20)
    year3= models.CharField(default='',max_length=20)
    yrfrom5= models.DateField(null=True)
    yrnav5= models.CharField(default='',max_length=20)
    year5= models.CharField(default='',max_length=20)
    incdate= models.DateField(null=True)
    incnav= models.CharField(default='',max_length=20)
    returnsinceinception= models.CharField(default='',max_length=20)
    launchdate= models.DateField(null=True)
    AUM= models.CharField(default='',max_length=20)
    AumDate= models.DateField(null=True)
    fundcode= models.CharField(default='',max_length=20)
    Fundmgr= models.CharField(default='',max_length=20)
    FundType= models.CharField(default='',max_length=20)
    NoOfStocks= models.CharField(default='',max_length=20)
    schemeType= models.CharField(default='',max_length=20)
    arqScore= models.CharField(default='',max_length=20)
    crisilRating= models.CharField(default='',max_length=20)
    valueRating= models.CharField(default='',max_length=20)
    mstarRating= models.CharField(default='',max_length=20)
    navdata=JSONField(default=dict)



#order book
class Order(models.Model):
	order_id = models.IntegerField(primary_key=True)
	scheme_code=models.ForeignKey(Scheme)
	user_id = models.IntegerField(null=False,blank=False)
	amc_code = models.CharField(default='',max_length=100)
	order_status = models.CharField(default='',max_length=10)

# order book details	
class OrderDetailsLive(models.Model):
	order_id = models.ForeignKey(Order)
	angel_id = models.CharField(max_length=10)
	user_type = models.CharField(max_length=5)
	ref_no = models.CharField(max_length=40)
	isin = models.CharField(max_length=15)
	trade_action = models.CharField(max_length=3)
	order_type = models.CharField(max_length=8) # buySell in api doc pdf v=1.6
	demat_flag = models.CharField(max_length=1)
	dp_id = models.CharField(max_length=20) # what?
	kyc_flag = models.CharField(max_length=1)
	min_redeemption_flag = models.CharField(max_length=1)
	cut_off_time = models.DateTimeField()
	amount = models.CharField(max_length=15)
	action = models.CharField(max_length=8)
	transaction_status = models.CharField(max_length=1)
	transaction_ref_no = models.CharField(max_length=40)
	limit_blocked = models.CharField(max_length=15)
	allunits = models.CharField(max_length=10)	
	fund_name = models.CharField(max_length=200)
	scheme_name=models.CharField(default='',max_length=200)
	order_date=models.DateTimeField(null=True, blank=True)


# order book details	
class OrderDetailsVirtual(models.Model):
	order_id = models.ForeignKey(Order)
	angel_id = models.CharField(max_length=10)
	user_type = models.CharField(max_length=5)
	ref_no = models.CharField(max_length=40)
	isin = models.CharField(max_length=15)
	trade_action = models.CharField(max_length=3)
	order_type = models.CharField(max_length=8) # buySell in api doc pdf v=1.6
	demat_flag = models.CharField(max_length=1)
	dp_id = models.CharField(max_length=20) # what?
	kyc_flag = models.CharField(max_length=1)
	min_redeemption_flag = models.CharField(max_length=1)
	cut_off_time = models.DateTimeField()
	amount = models.CharField(max_length=15)
	action = models.CharField(max_length=8)
	transaction_status = models.CharField(max_length=1)
	transaction_ref_no = models.CharField(max_length=40)
	limit_blocked = models.CharField(max_length=15)
	allunits = models.CharField(max_length=10)	
	fund_name = models.CharField(max_length=200)
	scheme_name=models.CharField(default='',max_length=200)
	order_date=models.DateTimeField(null=True, blank=True)




