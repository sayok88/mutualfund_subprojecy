from django.conf.urls import url,include
from . import views
from django.contrib.staticfiles.urls import static,staticfiles_urlpatterns
from django.conf import settings

urlpatterns = [
url(r'^top5schemes/', views.top5schemes, name='angel-login-view'),
url(r'^allschemes/', views.allschemes, name='angel-login-view'),
url(r'^api/v1/auth/login/$', views.LoginView.as_view(), name='login'),
url(r'^api/v1/auth/getauth/$', views.TesView.as_view(), name='login'),
url(r'^api/purchaseorder/$', views.PurchaseOrderView.as_view(), name='purchase-order'),
url(r'^api/v1/auth/logout/$', views.LogoutView.as_view(), name='logout'),
url(r'^api/v1/auth/orderbook/$', views.OrderBookView.as_view(), name='orderbook'),
url(r'^api/v1/auth/portfolio/$', views.PortfolioView.as_view(), name='portfolio'),
url(r'^', views.index, name='login-view'),
]