# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core.serializers import serialize
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from .models import *
import os, json, random, requests
from rest_framework import permissions, status, views, viewsets
from rest_framework.response import Response
from datetime import date

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

# Create your views here.
def index(request):
    return render(request,"index.html",{})

#========= top 5 schemes ============#
def top5schemes(request):
    ids=Scheme.objects.values_list('scheme_code',flat=True)
    n = 10
    rand_ids = random.sample(ids, n)
    random_records = SchemeDetails.objects.filter(scheme__scheme_code__in=rand_ids)
    scheme_data=[]
    for s in random_records:
        scheme_data.append({'scheme_name':s.scheme.scheme_name,
            'amc':s.scheme.amc_code.amc_name,'plan_name':s.scheme.plan_name,'aum':s.AUM,'ratings':s.valueRating,'returns':s.year1,'nav':s.nav,
            'week1':s.week1,'mth1':s.mth1,'mth3':s.mth3,'mth6':s.mth6,'year1':s.year1,'year3':s.year3,'year5':s.year5,'minInvestment':s.min_Pur_Amt_FrNewInvest,
            'option':s.optionName, 'navDate':s.navDate, 'sCutoffTimePurchase':s.sCutoffTimePurchase, 'totalUnits':s.NoOfStocks,
            'multipleOf':s.Pur_Mul_Amt_FrNewInvest, 'exchange':s.scheme.exchange,'scheme_code':s.scheme.scheme_code,'scheme_type':s.schemeType,
            'isin':s.scheme.isin})

    return JsonResponse({'data':scheme_data})

#================= all schemes =================#
def allschemes(request):
    random_records = SchemeDetails.objects.all().select_related()
    scheme_data=[]
    for s in random_records:

        scheme_data.append({'scheme_name':s.scheme.scheme_name,'plan_name':s.scheme.plan_name,'amc':s.scheme.amc_code.amc_name,
            'aum':s.AUM,'ratings':s.valueRating,'returns':s.year1,'nav':s.nav,'week1':s.week1,'mth1':s.mth1,'mth3':s.mth3,
            'mth6':s.mth6,'year1':s.year1,'year3':s.year3,'year5':s.year5,'minInvestment':s.min_Pur_Amt_FrNewInvest,
            'option':s.optionName, 'navDate':s.navDate, 'sCutoffTimePurchase':s.sCutoffTimePurchase, 'totalUnits':s.NoOfStocks,
            'multipleOf':s.Pur_Mul_Amt_FrNewInvest, 'exchange':s.scheme.exchange,'scheme_code':s.scheme.scheme_code,
            'scheme_type':s.schemeType,'isin':s.scheme.isin
            })
    return JsonResponse({'data':scheme_data})

class LoginView(views.APIView):
    def post(self, request, format=None):
        data = request.data

        username = data.get('username', None)
        password = data.get('password', None)

        account = authenticate(username=username, password=password)

        if account is not None:
            if account.is_active:
                login(request, account)

                serialized=json.loads( serialize('json',User.objects.filter(username=username)))
                request.session["user"]=username
                request.session.modified=True
                return Response({'auth':True,'fields':serialized})
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)
class TesView(views.APIView):
    """docstring for TesView"""
    def get(self, request,format=None):
        bleh="bleh"
        serialized=[]
        auth=False
        if "user" in request.session:
            username=request.session["user"]

            serialized=json.loads( serialize('json',User.objects.filter(username=username)))
            auth=True
        return Response({'auth':auth,'fields':serialized})
class PortfolioView(views.APIView):        
    def get(self,request,format=None):
        null=None
        true=True
        holdings_response={"data":{"portfolioList":[{"angelQty":"100","avgprice":"11","currMKTValue":"2502.00","exchange":"MF","lastNAVPrice":"25.0200","prevNAV":"25.6900","schemeName":"Axis Equity Fund - Growth","totalQty":"100","unrealizedPercentage":"127.45","unrealizedValue":"1402.00","yesterdayMKTValue":"2569.00","Scheme_Code":"17307","CoCode":"10869","ExchCode":"3","Exchange":"BSE","PlanName":"Equity Fund - T3","OptionName":"Growth","End_Date":"2099-12-31","End_time":"00:00","AMC_Code":"AMF","Min_Pur_Amt_FrNewInvest":"5000","Pur_Mul_Amt_FrNewInvest":"1.00","sCutoffTimePurchase":"1/1/1900 3:00:00 PM","sCutoffTimeRedemption":"1/1/1900 3:00:00 PM","Min_Redemption_Units":"1.0000","navdate":"2018-02-02","adjnav":"25.0200","prevnavdate":"2017-06-05","prevadjnav":"22.4800","ISIN":"INF846K01164"}],"totalCurrentValue":1660769.51,"totalDayGainLoss":-49975.81,"totalDayGainLossPer":-2.92,"totalUnrealizedPercentage":23.48,"totalUnrealizedValue":315838},"message":"","success":true}
        return Response(holdings_response)

class OrderBookView(views.APIView):        
    def get(self,request,format=None):
        angel_id='ABH1'
        password='fff111'
        null=None
        true=True
        orderbook_request={"data":[{"AllUnits":"","AllotedAmountUnits":0,"AmcCode":"RMF","AmountUnit":497.59,"BuySell":"","ClientCode":"P94485","CutOffTime":"","DematPhysical":"","DpId":"","DpcFlag":"","ExportedOn":"1/1/1900 12:00:00 AM","FolioNo":"477181128003","InsertedBy":"","InsertedOn":"11/9/2017 12:00:00 AM","InternalRefNo":"LFIGG477280578","IpAddress":"","Isin":"INF204K01UN9","KycFlag":"","LumSumRemarks":null,"LumsumRegistrationNo":"","MinRedemptionFlag":"","NavDate":"1/1/1900 12:00:00 AM","NavValue":0,"Orderstatus":"SUCCESS","PurchaseRedeem":"R","SbARNCode":"","SchemeCode":"RELLFTPI-GR-L0","SchemeName":"RELIANCE LIQUID FUND-TREASURY PLAN - PLAN GROWTH PLAN GROWTH OPT","TransactionStatus":"SUCCESS","UpdatedBy":"","UpdatedOn":"11/9/2017 12:00:00 AM","UserType":""}],"message":"fetched successfully","success":true}
        try:
            clientIP=get_client_ip(request)
            api_login=settings.ANGEL_BROKING_SVC_URL+'/login'
            api_orderbook=settings.ANGEL_BROKING_SVC_URL+'/mforderbook'
            login_request=requests.post(api_login, "{'angelID':'"+angel_id+"','password':'"+password+"','clientIP':'"+str(clientIP)+"','osType':'"+str(settings.OSTYPE)+"','osVersion':'"+str(settings.OSVERSION)+"','deviceID':'"+str(settings.DEVICEID)+"'}", verify=False)
            login_request=login_request.json()
            session_id=str(login_request["Login"]["sessionID"])
            group_id=str(login_request["Login"]["groupID"])
            user_code=str(login_request["Login"]["userCode"])
            login_message=str(login_request["Login"]["loginMessage"])
            login_allowed=str(login_request["Login"]["loginAllowed"])
            orderbook_request=requests.post(api_orderbook,"{'angelID':'ABH1','fromDate':'2017-08-06','toDate':'2018-02-21','userType':'SBO'}", verify=False)
            orderbook_request=orderbook_request.json()
            if "data" in orderbook_request:
                if not orderbook_request["data"]:
                    orderbook_request={"data":[{"AllUnits":"","AllotedAmountUnits":0,"AmcCode":"RMF","AmountUnit":497.59,"BuySell":"","ClientCode":"P94485","CutOffTime":"","DematPhysical":"","DpId":"","DpcFlag":"","ExportedOn":"1/1/1900 12:00:00 AM","FolioNo":"477181128003","InsertedBy":"","InsertedOn":"11/9/2017 12:00:00 AM","InternalRefNo":"LFIGG477280578","IpAddress":"","Isin":"INF204K01UN9","KycFlag":"","LumSumRemarks":null,"LumsumRegistrationNo":"","MinRedemptionFlag":"","NavDate":"1/1/1900 12:00:00 AM","NavValue":0,"Orderstatus":"SUCCESS","PurchaseRedeem":"R","SbARNCode":"","SchemeCode":"RELLFTPI-GR-L0","SchemeName":"RELIANCE LIQUID FUND-TREASURY PLAN - PLAN GROWTH PLAN GROWTH OPT","TransactionStatus":"SUCCESS","UpdatedBy":"","UpdatedOn":"11/9/2017 12:00:00 AM","UserType":""}],"message":"fetched successfully","success":true}
        except Exception as e:
            return Response({"data":[{"AllUnits":"","AllotedAmountUnits":0,"AmcCode":"RMF","AmountUnit":497.59,"BuySell":"","ClientCode":"P94485","CutOffTime":"","DematPhysical":"","DpId":"","DpcFlag":"","ExportedOn":"1/1/1900 12:00:00 AM","FolioNo":"477181128003","InsertedBy":"","InsertedOn":"11/9/2017 12:00:00 AM","InternalRefNo":"LFIGG477280578","IpAddress":"","Isin":"INF204K01UN9","KycFlag":"","LumSumRemarks":null,"LumsumRegistrationNo":"","MinRedemptionFlag":"","NavDate":"1/1/1900 12:00:00 AM","NavValue":0,"Orderstatus":"SUCCESS","PurchaseRedeem":"R","SbARNCode":"","SchemeCode":"RELLFTPI-GR-L0","SchemeName":"RELIANCE LIQUID FUND-TREASURY PLAN - PLAN GROWTH PLAN GROWTH OPT","TransactionStatus":"SUCCESS","UpdatedBy":"","UpdatedOn":"11/9/2017 12:00:00 AM","UserType":""}],"message":"fetched successfully","success":true})
        
        if "data" in orderbook_request:
            if not orderbook_request["data"]:
                orderbook_request={"data":[{"AllUnits":"","AllotedAmountUnits":0,"AmcCode":"RMF","AmountUnit":497.59,"BuySell":"","ClientCode":"P94485","CutOffTime":"","DematPhysical":"","DpId":"","DpcFlag":"","ExportedOn":"1/1/1900 12:00:00 AM","FolioNo":"477181128003","InsertedBy":"","InsertedOn":"11/9/2017 12:00:00 AM","InternalRefNo":"LFIGG477280578","IpAddress":"","Isin":"INF204K01UN9","KycFlag":"","LumSumRemarks":null,"LumsumRegistrationNo":"","MinRedemptionFlag":"","NavDate":"1/1/1900 12:00:00 AM","NavValue":0,"Orderstatus":"SUCCESS","PurchaseRedeem":"R","SbARNCode":"","SchemeCode":"RELLFTPI-GR-L0","SchemeName":"RELIANCE LIQUID FUND-TREASURY PLAN - PLAN GROWTH PLAN GROWTH OPT","TransactionStatus":"SUCCESS","UpdatedBy":"","UpdatedOn":"11/9/2017 12:00:00 AM","UserType":""}],"message":"fetched successfully","success":true}
        return Response(orderbook_request)

class LogoutView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        logout(request)

        return Response({}, status=status.HTTP_204_NO_CONTENT)
    
    
##============ purchase order api =============##
class PurchaseOrderView(views.APIView):
    #permission_classes = (permissions.IsAuthenticated,)
    
    def post(self,request,format=None):
        try:
            data = request.data
            amount = data.get('amount',None)
            limit = data.get('limit',None)
            mutf = data.get('mutf',None)
            angelID = None
            userType = None
            amc_code = None
            ref_no = None
            
            json_data ={
                'angelID':None,
                'userType':None,
                'refNo': None,
                'amcCode': None,
                'schemeCode': mutf['scheme_code'],
                'isin':mutf['isin'] ,
                'tradeAction':'P',
                'buySell':'FRESH', # buySell and orderType is same, orderType 1 == buySell FRESH
                'dmatFlag':None,
                'folio':'',
                'dpID':None,
                'kycFlag':'Y',
                'minRedemptionFlag':'N',
                'cutOffTime':mutf['sCutoffTimePurchase'],
                'amount':amount,
                'action':'insert',
                'transactionStatus':None,
                'transactionRefNo': None,
                'limitBlocked':limit,
                'allUnits':mutf['totalUnits'],
                'osType':str(settings.OSTYPE),
                'osVersion':str(settings.OSVERSION),
                'deviceID':str(settings.DEVICEID)
                }
            
            #======= getting response from mf purchase order api  ============#
            angel_url = settings.ANGEL_BROKING_SVC_URL
            purchase_url = angel_url + '/mfpurchaseorder'
            json_string = str(json_data)
            r = requests.post(purchase_url,json_string, verify=False)
            response = r.json()
            
            #======= getting response from mf order book api  ============#
            orderbook_url = angel_url + '/mforderbook'
            #{"angelID":"P94485","fromDate":"2017-08-06","toDate":"2017-11-13","userType":"SBO"}
            json_data = '{"angelID":"P94485","fromDate":"2017-08-06","toDate":"2017-11-13","userType":"SBO"}'
            r = requests.post(orderbook_url,json_data, verify=False)
            response = r.json()
            
            if response is not None:
                if response['success'] == True:
                    ##----------Save to Orderbook---------------##
                    order_data = Order(
                                    scheme_code = mutf['scheme_code'],
                                    user_id = user_id, # angel id and uderId is same here,
                                    amc_code =  None, #amc_code
                                    order_status = None
                                )
                    order_data.save()
                    order_id = order_data.order_id
                    ##----------Save to LiveOrder---------------##
                    live_oder_data = OrderDetailsLive(
                                        order_id = order_id,
                                        angel_id = angelID,
                                        user_type = userType,
                                        ref_no = ref_no,
                                        isin = mutf['isin'],
                                        trade_action = 'P',
                                        order_type = 'FRESH',
                                        demat_flag = null,
                                        dp_id = null,
                                        kyc_flag = 'Y',
                                        min_redeemption_flag = 'N',
                                        cut_off_time = mutf['sCutoffTimePurchase'],
                                        amount = amount,
                                        action = 'insert',
                                        transaction_status = null,
                                        transaction_ref_no = null,
                                        limit_blocked = limit,
                                        allunits = mutf['totalUnits'],
                                        fund_name = mutf['plan_name'],
                                        scheme_name = mutf['scheme_code'],
                                        order_date = datetime.now()
                                    )
                    live_oder_data.save()
                
            # redirect to portfolio order book
            
            return Response({"msg":"buy order successful"}, status=status.HTTP_200_OK )
        except Exception as ex:
            print ex
            return Response({},status=status.HTTP_400_BAD_REQUEST)




        